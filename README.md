# raspberrypi-hotspot-wifi
Make your raspberry pi a hotspot wifi

Run `curl https://raw.githubusercontent.com/JonthanLT/raspberrypi-hotspot-wifi/master/install.sh | sudo bash -s`

-------
Execute this one with two arguments after `sudo bash -s` to choose your SSID and WPAKey

Run `curl https://raw.githubusercontent.com/JonthanLT/raspberrypi-hotspot-wifi/master/install.sh | sudo bash -s [1] [2]`

*[1]* -> SSID

*[2]* -> WPAKey

*Default*
-------
*SSID: `MyPi3`*
*WPAKey: `raspberry`*
### Enjoy
