#!/bin/bash

if [ "$(whoami)" != "root" ]; then
    echo "Sorry, you are not root."
    exit 1
fi

ssid="MyPi3"
wpak="raspberry"

if [ $# -eq 1 ]
then
    ssid=$1
fi
if [ $# -eq 2 ]
then
    ssid=$1
    wpak=$2
fi

sudo locale-gen en_US.UTF-8

sudo apt update
echo iptables-persistent iptables-persistent/autosave_v4 boolean true | sudo debconf-set-selections
echo iptables-persistent iptables-persistent/autosave_v6 boolean true | sudo debconf-set-selections
sudo apt -y install dnsmasq hostapd iptables-persistent sshpass telnet
sudo cp /etc/dhcpcd.conf /etc/dhcpcd.conf.backup
sudo cat <<EOF > /etc/dhcpcd.conf
denyinterfaces wlan0
EOF

sudo cp /etc/network/interfaces /etc/network/interfaces.backup
sudo cat <<EOF > /etc/network/interfaces
allow-hotplug wlan0
iface wlan0 inet static
    address 172.24.1.1
    netmask 255.255.255.0
    network 172.24.1.0
    broadcast 172.24.1.255
#    wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf

auto eth0
allow-hotplug eth0
iface eth0 inet dhcp

EOF

sudo service dhcpcd restart
sudo ifdown wlan0; sudo ifup wlan0

sudo cat <<EOF > /etc/hostapd/hostapd.conf
# This is the name of the WiFi interface we configured above
interface=wlan0

# Use the nl80211 driver with the brcmfmac driver
driver=nl80211

# This is the name of the network
ssid=`echo $ssid`

# Use the 2.4GHz band
hw_mode=g

# Use channel 6
channel=6

# Enable 802.11n
ieee80211n=1

# Enable WMM
wmm_enabled=1

# Enable 40MHz channels with 20ns guard interval
ht_capab=[HT40][SHORT-GI-20][DSSS_CCK-40]

# Accept all MAC addresses
macaddr_acl=0

# Use WPA authentication
auth_algs=1

# Require clients to know the network name
ignore_broadcast_ssid=0

# Use WPA2
wpa=2

# Use a pre-shared key
wpa_key_mgmt=WPA-PSK

# The network passphrase
wpa_passphrase=`echo $wpak`

# Use AES, instead of TKIP
rsn_pairwise=CCMP
EOF

sudo cp /etc/hostapd/hostapd.conf /etc/hostapd/hostapd.conf.backup

sudo sudo cp /etc/default/hostapd /etc/default/hostapd.backup
cat <<EOF > /etc/default/hostapd
DAEMON_CONF="/etc/hostapd/hostapd.conf"
DAEMON_OPT=
EOF

sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig
sudo cat <<EOF > /etc/dnsmasq.conf
interface=wlan0      # Use interface wlan0
listen-address=172.24.1.1 # Explicitly specify the address to listen on
bind-interfaces      # Bind to the interface to make sure we aren't sending things elsewhere
server=8.8.8.8       # Forward DNS requests to Google DNS
domain-needed        # Don't forward short names
bogus-priv           # Never forward addresses in the non-routed address spaces.
dhcp-range=172.24.1.50,172.24.1.150,12h # Assign IP addresses between 172.24.1.50 and 172.24.1.150 with a 12 hour lease time
EOF

# Forwarding
sudo sh -c "echo 1 > /proc/sys/net/ipv4/ip_forward"
sudo cp /etc/sysctl.conf /etc/sysctl.conf.backup
sudo cat <<EOF > /etc/sysctl.conf
#
# /etc/sysctl.conf - Configuration file for setting system variables
# See /etc/sysctl.d/ for additional system variables.
# See sysctl.conf (5) for information.
#

#kernel.domainname = example.com

# Uncomment the following to stop low-level messages on console
#kernel.printk = 3 4 1 3

##############################################################3
# Functions previously found in netbase
#

# Uncomment the next two lines to enable Spoof protection (reverse-path filter)
# Turn on Source Address Verification in all interfaces to
# prevent some spoofing attacks
#net.ipv4.conf.default.rp_filter=1
#net.ipv4.conf.all.rp_filter=1

# Uncomment the next line to enable TCP/IP SYN cookies
# See http://lwn.net/Articles/277146/
# Note: This may impact IPv6 TCP sessions too
#net.ipv4.tcp_syncookies=1

# Uncomment the next line to enable packet forwarding for IPv4
net.ipv4.ip_forward=1

# Uncomment the next line to enable packet forwarding for IPv6
#  Enabling this option disables Stateless Address Autoconfiguration
#  based on Router Advertisements for this host
#net.ipv6.conf.all.forwarding=1


###################################################################
# Additional settings - these settings can improve the network
# security of the host and prevent against some network attacks
# including spoofing attacks and man in the middle attacks through
# redirection. Some network environments, however, require that these
# settings are disabled so review and enable them as needed.
#
# Do not accept ICMP redirects (prevent MITM attacks)
#net.ipv4.conf.all.accept_redirects = 0
#net.ipv6.conf.all.accept_redirects = 0
# _or_
# Accept ICMP redirects only for gateways listed in our default
# gateway list (enabled by default)
# net.ipv4.conf.all.secure_redirects = 1
#
# Do not send ICMP redirects (we are not a router)
#net.ipv4.conf.all.send_redirects = 0
#
# Do not accept IP source route packets (we are not a router)
#net.ipv4.conf.all.accept_source_route = 0
#net.ipv6.conf.all.accept_source_route = 0
#
# Log Martian Packets
#net.ipv4.conf.all.log_martians = 1
#
EOF

# IPTABLES
sudo cp /etc/init.d/firewall /etc/init.d/firewall.backup
sudo cat <<EOF > /etc/init.d/firewall
#!/bin/sh
### BEGIN INIT INFO
# Provides: firewall rules
# Required-Start: $remote_fs $syslog
# Required-Stop: $remote_fs $syslog
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: Start daemon at boot time
# Description: Enable service provided by daemon.
### END INIT INFO

#Suppression des règles précédentes
iptables -F
iptables -X

# Définition des règles
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
iptables -A FORWARD -i eth0 -o wlan0 -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -i wlan0 -o eth0 -j ACCEPT
EOF

sudo chmod +x /etc/init.d/firewall
sudo /etc/init.d/firewall

sudo cat <<EOF > /etc/rc.local
#!/bin/sh -e
#
# rc.local
#
# This script is executed at the end of each multiuser runlevel.
# Make sure that the script will "exit 0" on success or any other
# value on error.
#
# In order to enable or disable this script just change the execution
# bits.
#
# By default this script does nothing.

# Print the IP address
_IP=$(hostname -I) || true
if [ "$_IP" ]; then
  printf "My IP address is %s\n" "$_IP"
fi

/etc/init.d/firewall

exit 0
EOF

sudo cp /etc/hosts /etc/hosts.backup
sudo cat <<EOF > /etc/hosts
127.0.0.1	localhost `echo $ssid`
::1		localhost ip6-localhost ip6-loopback
ff02::1		ip6-allnodes
ff02::2		ip6-allrouters

127.0.1.1	`echo $ssid`
EOF

sudo cp /etc/hostname /etc/hostname.backup
sudo cat <<EOF > /etc/hostname
`echo $ssid`
EOF

sudo service hostapd start
sudo update-rc.d hostapd enable

sudo service dnsmasq start
sudo update-rc.d dnsmasq enable

sudo reboot
